// @flow
import fetch from 'node-fetch';

// import { Trip } from './schema/Trip';
// import { Vehicle } from './schema/Vehicle';

const users = {};
export default function socketEvents(io: any): void {
  io.sockets.on('connection', socket => {
    console.log(socket.id);
    io.to(socket.id).emit('checkOnline', 'Send name of user to "checkOnline" ');
    socket.on('checkOnline', async data => {
      const { name } = data;
      users[name] = socket.id;
      if (name.slice(0, 5) === 'admin') {
        socket.join('adminRoom');
      }
      console.log('connected', users);
      io.to(socket.id).emit('authorized', 'You are authorized !!!');
      // io.to(users.admin).emit('ledOn', { name });
      io.sockets.in('adminRoom').emit('ledOn', { name });
      fetch(`http://led.media-almaty.kz/profile/computers/on-off?name=${encodeURI(name)}&type=on`, {
        method: 'GET',
      });
    });
    socket.on('checkUsers', async data => {
      io.to(socket.id).emit('checkUsers', users);
    });
    socket.on('action', async data => {
      const { name, json } = data;
      console.log('action');

      console.log(name, json);
      io.to(users[name]).emit('action', json);
    });
    socket.on('screenShot', async data => {
      const { name, json } = data;
      console.log('screenShot');

      console.log(json);
      socket.to(users[name]).emit('screenShot', json);
    });
    socket.on('post', async data => {
      const { json } = data || {};
      console.log('POST');
      const response = fetch('http://httpbin.org/post', {
        method: 'POST',
        body: { json },
      });
      console.log(response);
    });

    socket.on('login', async data => {
      const { name, password } = data;
      // const response = await fetch('http://httpbin.org/post', {
      //   method: 'POST',
      //   body: { name },
      // });
      io.to(users[name]).emit('disconnect', 'lol');
      console.log('login', name, password);
    });

    socket.on('disconnect', async () => {
      if (socket.room) socket.leave(socket.room);
      Object.keys(users).forEach(async name => {
        if (users[name] === socket.id) {
          io.sockets.in('adminRoom').emit('ledOff', { name });
          delete users[name];
          fetch(
            `http://led.media-almaty.kz/profile/computers/on-off?name=${encodeURI(name)}&type=off`,
            {
              method: 'GET',
            }
          );
        }
      });
      console.log('users', users);
      console.log('disconnect', socket.id);
    });
  });
}
