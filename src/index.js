// @flow

import express from 'express';

import bodyParser from 'body-parser';
// $FlowFixMe
import morgan from 'morgan';
// $FlowFixMe
import socketEvents from './sockets';
// import schema from './schema/schema';
//
// import DB from './schema/db';
//

const PORT = process.env.PORT || 27123;
const app = express();
// $FlowFixMe
const io = require('socket.io')();

app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('tiny'));

app.use('/api', (req, res) => res.send('sdgsdhsdh'));

// app.use(
//   '/api',
//   parseUserToken,
//   graphqlHTTP(() => {
//     return {
//       schema,
//       graphiql: true,
//     };
//   })
// );

const server = app.listen(PORT, console.log(`App works on ${PORT}...`)); // eslint-disable-line no-console

io.attach(server);
socketEvents(io);

export default app;
